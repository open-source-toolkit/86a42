# AES加密算法C语言实现

## 简介

本仓库提供了一个AES加密算法的C语言实现，包含完整的测试代码，可以直接使用。该实现提供了加密和解密两个接口，方便用户直接将其添加到工程中使用。代码采用纯C语言编写，具有良好的可移植性。

## 功能特点

- **AES加密算法**：实现了AES（Advanced Encryption Standard）加密算法，支持128位密钥。
- **加密与解密接口**：提供了加密和解密两个接口，用户可以直接调用。
- **测试代码**：包含完整的测试代码，确保算法的正确性和可靠性。
- **纯C代码**：代码采用纯C语言编写，方便移植到不同的平台和环境中。

## 使用方法

1. **下载代码**：将本仓库的代码下载到本地。
2. **添加到工程**：将代码文件添加到你的C语言工程中。
3. **调用接口**：根据需要调用`aes_encrypt`和`aes_decrypt`接口进行加密和解密操作。
4. **编译运行**：编译并运行你的工程，确保代码正常工作。

## 示例代码

以下是一个简单的示例代码，展示了如何使用本仓库提供的AES加密和解密接口：

```c
#include "aes.h"

int main() {
    unsigned char key[16] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
    unsigned char plaintext[16] = {0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34};
    unsigned char ciphertext[16];
    unsigned char decryptedtext[16];

    // 加密
    aes_encrypt(plaintext, ciphertext, key);

    // 解密
    aes_decrypt(ciphertext, decryptedtext, key);

    // 输出结果
    printf("Plaintext:  ");
    for (int i = 0; i < 16; i++) {
        printf("%02x ", plaintext[i]);
    }
    printf("\n");

    printf("Ciphertext: ");
    for (int i = 0; i < 16; i++) {
        printf("%02x ", ciphertext[i]);
    }
    printf("\n");

    printf("Decrypted:  ");
    for (int i = 0; i < 16; i++) {
        printf("%02x ", decryptedtext[i]);
    }
    printf("\n");

    return 0;
}
```

## 注意事项

- 本实现仅支持128位密钥，不支持其他密钥长度。
- 请确保密钥和明文/密文的长度符合AES算法的要求。
- 在实际使用中，请根据具体需求进行适当的修改和优化。

## 贡献

欢迎大家提出问题和建议，或者提交Pull Request进行代码改进。

## 许可证

本项目采用MIT许可证，详情请参阅`LICENSE`文件。